package net.tngou.tnfs.action;

import java.io.IOException;
import java.util.HashMap;
import java.util.Map;

import javax.servlet.ServletException;

import net.tngou.tnfs.pojo.POJO;
import net.tngou.tnfs.pojo.User;
import net.tngou.tnfs.util.DigestMD;

public class LoginAction extends BaseAction {

	
	@Override
	public void execute() throws ServletException, IOException {

		if(request.isSubmit())
		{
			String account = request.getParameter("account");
			String password = request.getParameter("password");
			 password= DigestMD.MD5(password);
		    System.err.println(password);
			 User bean = new User();
			 
			 Map<String, Object> map = new HashMap<String, Object>();
			 map.put("account", account);
			 map.put("password", password);
			 
			User user = bean.get(map);
			if(user!=null)
			{
				session.setAttribute("user", user);
				sendRedirect(getDomain().getBase());//登录跳转
				
			}else
			{
				root.put("msg", "账户或者密码错误！");
				printFreemarker("default/login.ftl", root);
			}
			
			
		}else
		{
			printFreemarker("default/login.ftl", root);
		}

	}
	
	public void exit() {
		
		session.removeAttribute("user");
		sendRedirect(getDomain().getBase());//登录跳转
	}
}
