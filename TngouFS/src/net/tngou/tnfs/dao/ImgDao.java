package net.tngou.tnfs.dao;


import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;



import org.apache.commons.beanutils.BeanUtils;

import net.tngou.jtdb.Field;
import net.tngou.jtdb.Field.Type;
import net.tngou.jtdb.SortField;
import net.tngou.jtdb.TngouDBHelp;
import  net.tngou.tnfs.pojo.Img;
import net.tngou.tnfs.pojo.POJO;
import net.tngou.tnfs.util.PageUtil;
public  class ImgDao implements DaoImp<Img>{
	
  private final String TABLE="img";
	@Override
	public PageUtil query(int page, int size, SortField sortField, Field... fields) {
	
		TngouDBHelp help=TngouDBHelp.getConnection();
		net.tngou.jtdb.Page p = help.select(TABLE, sortField, page, size, fields);
		 List<Map<String, Object>> list = p.getList();
		 List<Img> imgs = new ArrayList<Img>();
		 list.forEach(e->{
			 Img img = new Img();
			 try {
				BeanUtils.populate(img, e);
				imgs.add(img);
			} catch (Exception e1) {
				// TODO Auto-generated catch block
				e1.printStackTrace();
			}
			 
		 });
			
		return new PageUtil(imgs, page, size, p.getTotal());
	}

	

	@Override
	public void save(Img img) {
		// TODO Auto-generated method stub
		
		TngouDBHelp help=TngouDBHelp.getConnection();
		
		
		Field[] fields = new Field[10];
		fields[0] = new Field("id", img.getId(), Type.Key);
//		fields[1] = new Field("path", img.getPath(), Type.String);
		fields[1] = new Field("src", img.getSrc(), Type.String);
		fields[2] = new Field("width", img.getWidth()+"", Type.String);
		fields[3] = new Field("height", img.getHeight()+"", Type.String);
		fields[4] = new Field("size", img.getSize()+"", Type.String);
		fields[5] = new Field("format", img.getFormat(), Type.String);
		fields[6] = new Field("title", img.getTitle(), Type.Text);
		fields[7] = new Field("message", img.getMessage(), Type.Text);
		fields[8] = new Field("tag", img.getTag(), Type.String);
		fields[9] = new Field("time", img.getTime()+"", Type.String);
		help.insert(TABLE, fields );
		help.closeConnection();
		
	}



	

	
	
}
