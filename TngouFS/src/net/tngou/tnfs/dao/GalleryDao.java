package net.tngou.tnfs.dao;

import java.util.List;




import net.tngou.tnfs.jdbc.QueryHelper;
import net.tngou.tnfs.pojo.Gallery;
import net.tngou.tnfs.pojo.Galleryclass;
import net.tngou.tnfs.pojo.POJO;

public class GalleryDao extends BaseDao{

	
	
	
	public int pictureSize(int date) {
		
		String sql="SELECT SUM(size) FROM `"+Prefix+"gallery` WHERE DATE_SUB(CURDATE(),INTERVAL ? DAY) <=time ";
		
		 return (int) QueryHelper.stat(sql, date);
	}
	
	public int gallerySize(int date) {
		String sql="SELECT count(*) FROM `"+Prefix+"gallery` WHERE DATE_SUB(CURDATE(),INTERVAL ? DAY) <=time ";
		
		 return (int) QueryHelper.stat(sql, date);
	}
	
	
	public List<Gallery> getGallerie(long id,int rows,int type) {
		
		Galleryclass galleryclass = new Galleryclass().get(type);
		
		String sql="SELECT * FROM `"+Prefix+"gallery` WHERE id>? ORDER BY id ASC";
		if(galleryclass!=null)
			sql="SELECT * FROM `"+Prefix+"gallery` WHERE id>? AND galleryclass="+galleryclass.getId()+" ORDER BY id ASC";
		return QueryHelper.query_slice(Gallery.class, sql,1,rows,id);
	}
	
	public int getGallerie(long id,int type) {
		Galleryclass galleryclass = new Galleryclass().get(type);
		String sql="SELECT COUNT(*) FROM `"+Prefix+"gallery` WHERE id>? ";
		if(galleryclass!=null)
			sql="SELECT COUNT(*) FROM `"+Prefix+"gallery` WHERE id>? AND galleryclass="+galleryclass.getId();
		return (int) QueryHelper.stat(sql, id);
		
		
	}
	
}
