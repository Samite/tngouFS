package net.tngou.tnfs.quartz;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.Collection;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Set;

import org.apache.commons.lang3.ArrayUtils;














import net.sf.ehcache.Element;
import net.tngou.tnfs.cache.CacheEngine;
import net.tngou.tnfs.cache.EhCacheEngine;
import net.tngou.tnfs.enums.CacheEnum;
import net.tngou.tnfs.enums.CountEnum;
import net.tngou.tnfs.enums.TTypeEnum;
import net.tngou.tnfs.jdbc.QueryHelper;
import net.tngou.tnfs.pojo.Visits;


/**
 * ，主要处理浏览器的相关信息
 * @author 陈磊
 *
 */
public class CacheUtil 
{
	private static String fullyQualifiedName=null;
	private static String type=null;
	private static CacheEngine cacheEngine =new  EhCacheEngine().getInstance();
	
	public static  CacheUtil getInstance(CacheEnum cacheEnum,CountEnum countEnum){
		fullyQualifiedName=cacheEnum.toString();
		type=countEnum.toString();
		
		return new CacheUtil();
		
	}
	
	
	private CacheUtil() {
		// TODO Auto-generated constructor stub
	}
	
	/**
	 * 添加浏览量，++1
	 * @param id 是该信息的id
	 * @param type 主要是存放该信息的数据库表名
	 */
	public  void Add(long id,TTypeEnum tTypeEnum)
	{
		Serializable key = tTypeEnum.toString()+id;
		Visits visit=(Visits) cacheEngine.get(fullyQualifiedName, key);
		if(visit==null)
		{
			visit = new Visits();
			visit.setId(id);
			visit.setCount(1);
			visit.setType(tTypeEnum.toString());
			cacheEngine.add(fullyQualifiedName, key, visit);
		}else {
			visit.setCount(visit.getCount()+1);
			cacheEngine.add(fullyQualifiedName, key, visit);
		}
		
	}
	
	
	public  void Add(long id,TTypeEnum tTypeEnum,int size)
	{
		Serializable key = tTypeEnum.toString()+id;
		Visits visit=(Visits) cacheEngine.get(fullyQualifiedName, key);
		if(visit==null)
		{
			visit = new Visits();
			visit.setId(id);
			visit.setCount(size);
			visit.setType(tTypeEnum.toString());
			cacheEngine.add(fullyQualifiedName, key, visit);
		}else {
			visit.setCount(visit.getCount()+size);
			cacheEngine.add(fullyQualifiedName, key, visit);
		}
		
	}
	
	
	public  void Update()
	{
		
		//List<Object[]> list = new ArrayList<Object[]>();
		Map<String, List<Object[]>> map =new  HashMap<String, List<Object[]>>();
		
		Collection<Element> elements = cacheEngine.getValues(fullyQualifiedName);
		
		for (Element element : elements) {
			Visits visit=(Visits) element.getObjectValue();
			List<Object[]> list = map.get(visit.getType());
			if(list==null)
			{
				list = new ArrayList<Object[]>();
				Object[] e={visit.getCount(),visit.getId()};
				list.add(e);
				map.put(visit.getType(), list);
				
			}else {
				Object[] e={visit.getCount(),visit.getId()};
				list.add(e);
				map.put(visit.getType(), list);
				
			}
			
		}
		
		
		Set<String> keys = map.keySet();
		for (String string : keys) 
		{
			List<Object[]> list = map.get(string);
			_update(list, string);
		}
		
		cacheEngine.remove(fullyQualifiedName);//清除
		
		
	}
	
	
	private  void _update(List<Object[]> list,String Table)
	{
		String sql = "UPDATE "+Table+" SET "+type+"="+type+"+? WHERE id=?";
		
		Object[] params = list.toArray() ;
		
		Object[][] arr=null;
		for (int i = 0; i < params.length; i++) {
			arr=ArrayUtils.add(arr, (Object[]) params[i]);
		}
		QueryHelper.batch(sql , arr);
		
	}
	

	
	
}
