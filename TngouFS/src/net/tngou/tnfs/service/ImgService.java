package net.tngou.tnfs.service;







import java.io.File;
import java.io.IOException;
import java.util.Date;



import java.util.List;

import net.tngou.jtdb.SortField;
import net.tngou.jtdb.SortField.Order;
import net.tngou.jtdb.TngouDBHelp;
import net.tngou.tnfs.dao.ImgDao;
import net.tngou.tnfs.pojo.Img;
import net.tngou.tnfs.pojo.POJO;
import net.tngou.tnfs.util.DigestMD;
import net.tngou.tnfs.util.HttpConfig;
import net.tngou.tnfs.util.PageUtil;

import org.apache.sanselan.ImageInfo;
import org.apache.sanselan.ImageReadException;
import org.apache.sanselan.Sanselan;

import ch.qos.logback.core.subst.Token.Type;

public class ImgService extends BaseService{
	
	
	
	public  void saveDB (String src,String title,String message,String tag) {
		
		Img img = new Img();
		File file = new File(HttpConfig.getInstance().getTnfspath()+src);
		if(!file.exists()) return;
	  if(file!=null)
	  {
		  img.setSize(file.length());
			try {
				ImageInfo imageInfo = Sanselan.getImageInfo(file);
				int height=imageInfo.getHeight();
				int width=imageInfo.getWidth();
				 String format=imageInfo.getFormat().name;
				 img.setFormat(format);
				 img.setHeight(height);
				 img.setWidth(width);
				 img.setSrc(src);
				 img.setTag(tag);
				 img.setTitle(title);
				 img.setMessage(message);
				 img.setTime(new Date().getTime());
				 img.setId(DigestMD.MD5(src));
			} catch (ImageReadException | IOException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
		  new ImgDao().save(img);
		 
//		  TngouDBHelp.getConnection().close();
	  } 
	}
	
	public static void main(String[] args) {
//		saveDB("/news/1.jpg", "title", "message", "news");
		
		
		SortField sortField = new SortField("time", Order.DESC);
		PageUtil page = new ImgDao().query(1, 10, sortField );
		List<Img> list = (List<Img>) page.getList();
		list.forEach(System.out::println);
		 TngouDBHelp.getConnection().close();
		
	}

}
