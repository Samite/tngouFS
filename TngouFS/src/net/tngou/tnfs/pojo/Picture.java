package net.tngou.tnfs.pojo;

/**
 * 
 * @author 陈磊
 *
 */
public class Picture extends POJO 
{
	private int gallery; //图片库
	private String src; //图片地址
	public int getGallery() {
		return gallery;
	}
	public void setGallery(int gallery) {
		this.gallery = gallery;
	}
	public String getSrc() {
		return src;
	}
	public void setSrc(String src) {
		this.src = src;
	}
	
	@Override
	protected boolean isObjectCachedByID() {
		
		return true;
	}
}
