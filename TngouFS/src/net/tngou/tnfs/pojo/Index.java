package net.tngou.tnfs.pojo;

import java.io.Serializable;

import net.tngou.tnfs.util.PageUtil;

public class Index  implements Serializable{

	
	private int gallerysize; //图库数
	private int picturesize; //照片书
	private int gallerysizeweek; //周更新图库数
	private int picturesizeweek; //周更新照片书
	private PageUtil page;
	public int getGallerysize() {
		return gallerysize;
	}
	public void setGallerysize(int gallerysize) {
		this.gallerysize = gallerysize;
	}
	public int getPicturesize() {
		return picturesize;
	}
	public void setPicturesize(int picturesize) {
		this.picturesize = picturesize;
	}
	public int getGallerysizeweek() {
		return gallerysizeweek;
	}
	public void setGallerysizeweek(int gallerysizeweek) {
		this.gallerysizeweek = gallerysizeweek;
	}
	public int getPicturesizeweek() {
		return picturesizeweek;
	}
	public void setPicturesizeweek(int picturesizeweek) {
		this.picturesizeweek = picturesizeweek;
	}
	public PageUtil getPage() {
		return page;
	}
	public void setPage(PageUtil page) {
		this.page = page;
	}
	
	
}
